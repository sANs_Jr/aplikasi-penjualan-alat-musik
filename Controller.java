package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.IOException;


public class Controller{
    ObservableList<String> pilihan = FXCollections.observableArrayList("Gitar","Drum","Keyboard","Piano","Saxophone","Trumpet","Bass");
    @FXML
    private ComboBox menualat;
    @FXML
    private Button tambah;
    @FXML
    private Button batal;
    @FXML
    private TextField nama_pem;
    @FXML
    private TextField alamat_pem;
    @FXML
    private TextField no_hp;
    @FXML
    private TextField banyak;
    @FXML
    private TextField bayar;
    @FXML
    private TextField kembalian;
    @FXML
    private TextField total;
    @FXML
    private TableView<Pembeli> tableView;
    @FXML
    private TableColumn<Pembeli, String> KolNama;
    @FXML
    private TableColumn<Pembeli, String> KolAlamat;
    @FXML
    private TableColumn<Pembeli, String> KolNo_HP;
    @FXML
    private TableColumn<Pembeli, String> KolDibeli;
    @FXML
    private TableColumn<Pembeli, Integer> KolBanyak;
    @FXML
    private TableColumn<Pembeli, Integer> KolTotal;
    @FXML
    private Button hitung;
    @FXML
    public void ButtonPressed(Event evt) throws IOException {
        menualat.setItems(pilihan);
        if (evt.getSource().equals(hitung)){
            Integer harga1=HargaAlat();
            AlatMusik baru=new AlatMusik((String)menualat.getValue(),harga1);
            Integer banyak1,total1;
            banyak1=Integer.parseInt(banyak.getText());
            total1=baru.harga*banyak1;
            total.setText(String.valueOf(total1));
        }
    }
    @FXML
    public void TextInputted(){

    }
    @FXML
    public Integer HargaAlat(){
        if(menualat.getValue().equals("Gitar")){
            return 1750000;
        }else if(menualat.getValue().equals("Drum")){
            return 12350000;
        }else if(menualat.getValue().equals("Keyboard")){
            return 7250000;
        }else {
            return 1000;
        }
    }
    @FXML
    private void Transaksi(){
//        Pembeli custom=new Pembeli((String)nama_pem.getText(),(String)alamat_pem.getText(),(String)no_hp.getText());
        AlatMusik baru=new AlatMusik((String)menualat.getValue(),HargaAlat());
        Integer bayar1,kembalian1,total1,banyak1;
        bayar1=Integer.parseInt(bayar.getText());
        banyak1=Integer.parseInt(banyak.getText());
        total1=banyak1*baru.harga;
//        total.setText(String.valueOf(total1));
        if(bayar1>=total1){
            kembalian1=bayar1-total1;
            kembalian.setText(String.valueOf(kembalian1));
            ObservableList<Pembeli> hasiltrans=FXCollections.observableArrayList();
            hasiltrans.add(new Pembeli((String)nama_pem.getText(),(String)alamat_pem.getText(),(String)no_hp.getText(),(String)baru.nama,banyak1,total1));
            tableView.setItems("D");
        }else{
            kembalian.setText("Uang Anda Kurang!");
        }
    }
}
